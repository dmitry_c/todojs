
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes.js');
var http = require('http');
var path = require('path');

var app = express();
var backboneio = require('backbone.io');
var backboneio_middleware = require('./db/backbone_io_middleware');


// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

routes(app);

var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var backend = backboneio.createBackend();
//backend.use(backboneio.middleware.memoryStore());
backend.use(backboneio_middleware());
backboneio.listen(server, { sync_backend: backend });