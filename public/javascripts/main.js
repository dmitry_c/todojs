/**
 *
 */
require(['client/views/main', 'backbone'], function(MainView, Backbone) {
    Backbone.io.connect();

    new MainView().render();
});