var helpers = require('./helpers');


module.exports = function () {

    return function (req, res, next) {
        var id = '_id';
        var crud = {
            create: function () {
                var model = req.model;

                helpers.create(model).then(function (doc) {
                    res.end(doc);
                });
            },

            read: function () {
                if (req.model[id]) {
                    helpers.read(req.model[id]).then(function (doc) {
                        res.end(doc);
                    });
                } else {
                    helpers.read().then(function (docs) {
                        res.end(docs);
                    });
                }
            },

            update: function () {
                helpers.update(req.model).then(function (doc) {
                    res.end(doc);
                });
            },

            delete: function () {
                helpers.delete(req.model).then(function () {
                    res.end(req.model);
                });
            }
        };

        if (!crud[req.method]) return next(new Error('Unsuppored method ' + req.method));
        crud[req.method]();
    }
};