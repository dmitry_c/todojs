/**
 * Sets up the database connection and exposes collections.
 *
 */
var mongo = require('mongodb'),
    MongoClient = mongo.MongoClient,
    ObjectId = mongo.ObjectID,
    config = require('../config'),
    Promise = require('node-promise').Promise,
    _ = require('underscore'),
    format = require('util').format;

module.exports = {};

MongoClient.connect(config.mongo_host, function (err, db) {
    if (err) {
        console.warn(format("MongoClient says: %s", err.message));
        process.exit(1);
    } else {
        exposeCollections(db);
    }
});


function exposeCollections(db) {
    var todos = db.collection('todos'),
        counter = db.collection('counter');

    function Counter() {
    }

    Counter.prototype = counter;
    Counter.prototype.advance = function (scope) {
        var resp = new Promise();
        this.findAndModify({_id: scope},
            [
                ['_id', 'asc']
            ],
            {$inc: {counter: 1024}},
            {new: true, upsert: true},
            function (err, obj) {
                if (err) {
                    resp.reject(err);
                } else {
                    resp.resolve(obj['counter']);
                }
            });

        return resp;
    };

    module.exports = _.extend(module.exports, {
        db: db,
        Counter: new Counter,
        Todos: todos,
        ObjectId: ObjectId,
        ObjectID: ObjectId
    });

    return module.exports;
}
