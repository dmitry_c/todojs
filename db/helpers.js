var db = require('./index'),
    Promise = require('node-promise').Promise,
    _ = require('underscore');


function createTodo(data) {
    var res = new Promise();

    db.Counter.advance('todos').then(function (counter) {
        console.log(counter);
        data.number = counter;
        data.status = 0;

        db.Todos.insert(data, function (err, itemsCreated) {
            if (err || itemsCreated.length != 1) {
                res.reject(err);
            } else {
                res.resolve(itemsCreated[0]);
            }
        });
    });
    return res;
}


function findTodos(id) {
    var query = {};
    if (id) {
        query['_id'] = new db.ObjectId(id);
    }
    var res = new Promise;

    // latest first
    var cursor = db.Todos.find(query).sort({number: -1});
    cursor.toArray(function (err, results) {
        if (err) {
            res.reject(err)
        } else {
            res.resolve(results);
        }
    });

    return res;
}


function updateTodo(item) {
    var res = new Promise;

    var id = item['_id'],
        update = _.pick(item, 'text', 'status', 'number');

    if (!id) {
        return res.reject({message: "_id required"});
    }

    db.Todos.update({_id: new db.ObjectId(id)},
        {$set: update},
        {w: 1},
        function (err, itemsUpdated) {

            if (err || itemsUpdated != 1) {
                res.reject(err);
            } else {
                res.resolve(item, itemsUpdated);
            }
        });

    return res;
}


function deleteTodo(item) {
    var res = new Promise;
    var id = item['_id'];
    if (!id) {
        return res.reject({message: "_id required"});
    }

    db.Todos.remove({_id: new db.ObjectId(id)}, function (err, removed) {
        if (err || removed != 1) {
            res.reject(err);
        } else {
            res.resolve(removed);
        }
    });
    return res;
}


function errorHandler(res, message) {
    return function (err) {
        console.warn(err.message);
        res.status(500).send({status: message});
    }
}


module.exports = {
    create: createTodo,
    read: findTodos,
    update: updateTodo,
    delete: deleteTodo,

    createTodo: createTodo,
    findTodos: findTodos,
    updateTodo: updateTodo,
    deleteTodo: deleteTodo,

    errorHandler: errorHandler
};