var controllers = require('./controllers');

/**
 * Binds the routes to the passed `app`
 *
 * @param app instance of Express application
 */
module.exports = function(app) {
    // start page
    app.get('/', controllers.home.index);

    // REST routes
    app.post('/api/todos/', controllers.todos.create);
    app.get('/api/todos/', controllers.todos.retrieve);
    app.get('/api/todos/:id/', controllers.todos.retrieve);
    app.put('/api/todos/:id/', controllers.todos.update);
    app.delete('/api/todos/:id/', controllers.todos.delete);

};
