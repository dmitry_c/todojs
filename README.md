## WT?
yet another TODO MVC with node.js & Backbone

## Prerequisites
* node.js with npm
* bower (npm install -g bower)
* grunt (npm install -g grunt-cli)
* mongodb running at port 27017

## Project Setup
```
git clone REPOSITORY_PATH
cd todojs
npm install
bower install
# some statics include js scripts from node_modules and bower_components,
# just create symlinks to those directories in public
ln -s ./bower_components/ ./public/bower_components
ln -s ./node_modules/ ./public/node_modules
# application loads client side mustache templates dynamically,
# so we symlink that too
ln -s ./client/ ./public/client
```

## Start server
cd PROJECT_ROOT

node app.js

open http://localhost:3000/

## Build minified js
cd PROJECT_ROOT

grunt watch

or manually: node tools/r.js -o build.js
