/**
 * Start page
 */
module.exports = (function() {
    var BOWER_ROOT = '/bower_components/';
    var MODULES = '/node_modules/';

    return {
        index: function(req, res){
            res.render('index', {
                title: 'TODO app',
                static_css: [
                    "/stylesheets/style.css",
                    BOWER_ROOT + 'bootstrap/dist/css/bootstrap.min.css'
                ],
                // !the order matters
                static_js: [
                    BOWER_ROOT + 'jquery/jquery.js',
                    BOWER_ROOT + 'jquery-ui/ui/minified/jquery-ui.min.js',
                    // underscore should go before backbone
                    MODULES + 'underscore/underscore.js',
                    BOWER_ROOT + 'backbone/backbone.js',

                    '/socket.io/socket.io.js',
                    '/socket.io/backbone.io.js',

                    "javascripts/built.min.js"
                ]
            });

        }
    };
})();