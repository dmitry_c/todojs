/**
 * REST handlers.
 *
 * this is unfinished and actually has no use
 * since everything handled through socket.io and backbone.io middleware.
 */

var helpers = require('../db/helpers');

module.exports = {
    /**
     * POST /api/todos/:id/
     */
    create: function(req, res) {
        helpers.createTodo(req.body).then(function(itemsCreated) {
            res.status(204).send({ok: true, created: itemsCreated});
        }, helpers.errorHandler(res, "Error creating todo"));
    },

    /**
     * GET /api/todos/
     */
    retrieve: function(req, res){
        helpers.findTodos(req.params.id).then(function(results){
            res.send(results);
        }, helpers.errorHandler(res, "Error retrieving todos"));
    },

    /**
     * PUT /api/todos/:id/
     */
    update: function(req, res) {
        // TODO
    },

    /**
     * DELETE /api/todos/:id/
     */
    delete: function(req, res) {
        // TODO
    }

};