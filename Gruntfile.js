module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        requirejs: {
            compile: {
                options: {
                    almond: true,
                    paths: {
                        main: 'public/javascripts/main',
                        jquery: 'almond_adapters/jquery',
                        jqueryui: 'empty:',
                        underscore: 'almond_adapters/underscore',
                        backbone: 'almond_adapters/backbone',
                        mustache: 'bower_components/mustache/mustache',
                        socketio: 'empty:',
                        backboneio: 'empty:'
                    },

                    name: 'main',
                    out: 'public/javascripts/built.min.js'
                }
            }
        },
        watch: {
          files: ['client/**/*.js', 'public/javascripts/main.js'],
          tasks: ['requirejs']
        }
    });

    grunt.loadNpmTasks('grunt-requirejs');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['requirejs']);
};
