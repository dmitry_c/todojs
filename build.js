({
    // almond.js does not work with shim,
    // so there are several dummy modules in almond_adapters directory which
    // aimed to bind module names to libs

//    shim: {
//        'socketio': {
//            exports: 'io'
//        },
//        'underscore': {
//            exports: '_'
//        },
//        'backbone': {
//            deps: [
//                'underscore',
//                'jquery'
//            ],
//            exports: 'Backbone'
//        }
//    },

    paths: {
        main: 'public/javascripts/main',
        jquery: 'almond_adapters/jquery',
        jqueryui: 'empty:',
        underscore: 'almond_adapters/underscore',
        backbone: 'almond_adapters/backbone',
        mustache: 'bower_components/mustache/mustache',
        socketio: 'empty:',
        backboneio: 'empty:'
    },

    name: 'main',
    out: 'public/javascripts/built.min.js'
})
