/**
 * Todo task Model & Collection.
 *
 * @return STATES the allowed task states
 * @return TodoItem task model
 * @return TodoList task collection
 */
define(['backbone'], function (Backbone) {
    Backbone.Model.prototype.idAttribute = '_id';  // it's '_id' in mongo object

    var State = {
        PENDING: -1,
        NEW: 0,
        RESOLVED: 1,
        DIRTY: 11
    };

    /**
     * The todo task model with attributes:
     * - text {String} task body
     * - number {number} unique number used for sorting, applied on server
     * - status {number} task state
     *
     * @type {Backbone.Model.extend|*}
     */
    var TodoItem = Backbone.Model.extend({
        defaults: {
            text: "",
            number: 0,
            status: State.PENDING
        },

        validate: function (attrs, options) {
            if (!attrs.text) {
                return "text is empty";
            }
        }

    });

    /**
     * The collection of todo task items.
     *
     * @type {Backbone.Collection.extend|*}
     */
    var TodoList = Backbone.Collection.extend({
        backend: 'sync_backend',  // backbone.io backend
        model: TodoItem,

        initialize: function () {
            this.bindBackend();  // setup backbone.io listeners
        },

        normalize: function () {
            this.models = _.flatten(_.map(this.sortedGroups(), function (pair) {
                return pair[1];
            }));
            return this;
        },

        moveItem: function (startIndex, endIndex) {
            this.models.splice(endIndex, 0, this.models.splice(startIndex, 1)[0]);
        },

        swapItems: function (index1, index2) {
            this.models[index1] = this.models.splice(index2, 1, this.models[index1])[0];
        },

        sortedGroups: function () {
            var sorter = function (item) {
                return -item.get('number');
            };

            return _.sortBy(_.map(_.pairs(this.groupBy('status')), function (pair) {
                return [pair[0], _.sortBy(pair[1], sorter)];
            }), function (pair) {
                return Number(pair[0])
            });
        },

        newTasks: function () {
            return _.object(this.sortedGroups())[State.NEW];
        },

        /**
         * Exchange models in collection and persist resulting items order.
         *
         * @param index1
         * @param index2
         */
        itemsSwapped: function (index1, index2) {
            this.swapItems(index1, index2);
            var coll = this.models;
            var item1 = coll[index1],
                item2 = coll[index2];
            var num1 = item1.get('number'),
                num2 = item2.get('number');

            item1.save({number: num2}, {
                success: function () {
                    item2.save({number: num1});
                }
            });

        },

        /**
         * Move the item at index `startIndex` in place of item with `endIndex`,
         * and persist new item number.
         *
         * @param startIndex
         * @param endIndex
         */
        itemDragged: function (startIndex, endIndex) {
            var coll = this.normalize().models;
            var itemNum = function (idx) {
                var n = +coll[0].isNew();
                if (idx === n - 1) return itemNum(n) + 0.5;
                if (idx === this.newTasks().length + n) return 0;
                return coll[idx].get('number');
            }.bind(this);

            if (Math.abs(startIndex - endIndex) === 1) {
                // 2 calls but potentially avoiding fractional item numbers
                return this.itemsSwapped(startIndex, endIndex);
            }

            var orderMark = startIndex > endIndex ? 0 : 1,
                targetNum = itemNum(endIndex + orderMark) + 0.5 * (itemNum(endIndex - 1 + orderMark) - itemNum(endIndex + orderMark));

            coll[startIndex].save({number: targetNum},
                {
                    success: function () {
                        this.moveItem(startIndex, endIndex);
                    }.bind(this)
                });
        }
    });


    return {
        STATES: State,
        TodoItem: TodoItem,
        TodoList: TodoList
    };
});