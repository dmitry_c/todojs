/**
 * Factory for task list view.
 * This is basically repeater for TodoItemView.
 */
define(['backbone', 'mustache', '../models/todos', '../views/TodoItemView', 'jquery'],
    function (Backbone, mustache, models, TodoItemView, $) {

        return Backbone.View.extend({
            model: new models.TodoList(),

            tagName: 'ul',
            className: 'list-group bb-items-list',

            initialize: function () {
                var self = this;

                this.model.on('add', this.render, this);
                this.model.on('change', this.render, this);
                this.model.on('remove', this.render, this);

                // event from socket
                this.model.on('backend', this.render, this);

                this.model.fetch({
                    success: function () {
                        self.render();
                    },
                    error: function () {
                        console.log('fetch failed');
                    }
                });

                // setup sortable
                $(self.$el).sortable({
                    items: 'li:not(.bb-list-item-resolved,.bb-item-break,.bb-list-item-pending)',  // drag only new tasks
                    start: function (event, ui) {
                        ui.item.startPos = ui.item.index();
                    },
                    stop: function (event, ui) {
                        self.model.itemDragged(ui.item.startPos, ui.item.index());
                    }
                });
            },

            breaker: $('<li></li>').addClass('bb-item-break').text('resolved tasks'),

            render: function () {
                var self = this;

                var addSubview = function (item) {
                    var subview = new TodoItemView({model: item}).render();
                    self.$el.append(subview.$el);
                };
                self.$el.html('');

                _.each(this.model.sortedGroups(), function (pair) {
                    var state = pair[0],
                        values = pair[1];

                    if (state == models.STATES.RESOLVED) {
                        self.$el.append(self.breaker);
                    }
                    _.each(values, addSubview);
                });

                return this;
            }
        });
    });