/**
 * The main view
 * contains "create" button and task list view
 */
define(['backbone', 'mustache', '../models/todos', '../views/TodoListView', 'jquery'],
    function (Backbone, mustache, models, TodoListView, $) {

        var collection = new models.TodoList();
        var listView = new TodoListView({model: collection});

        var templateReady = $.get('/client/templates/main.mustache');

        return Backbone.View.extend({
            el: $('#main_container'),
            events: {
                'click .bb-create-task': 'handleAddTask'
            },

            initialize: function () {
                this.templateReady = templateReady.then(function (resp) {
                    this.template = mustache.compile(resp);
                }.bind(this));
            },

            render: function () {
                this.templateReady.done(function () {
                    this.$el.html(this.template());
                    this.$el.append(listView.render(false).$el);
                }.bind(this));

                return this;
            },

            handleAddTask: function (ev) {
                ev.preventDefault();
                if (!collection.length || !collection.at(0).isNew()) {
                    collection.add(new models.TodoItem, {at: 0});
                }
            }
        });

    });