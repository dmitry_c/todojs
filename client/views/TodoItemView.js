/**
 * Factory for task item view.
 */
define(['backbone', 'mustache', '../models/todos', 'jquery'], function (Backbone, mustache, models, $) {

    var templateReady = $.get('/client/templates/TodoItem.mustache');

    var TodoItemView = Backbone.View.extend({
        model: new models.TodoItem(),
        tagName: 'li',
        className: 'list-group-item bb-list-item',

        events: {
            'click .bb-create-item': 'handleSave',
            'click .bb-cancel': 'handleCancel',
            'click .bb-remove-item': 'handleRemove',
            'click .bb-switch-status': 'handleSwitchStatus',
            'keypress textarea': 'handleTextChange',
            'keyup textarea': 'handleTextChange'
        },

        initialize: function () {
            this.templateReady = templateReady.then(function (resp) {
                this.template = mustache.compile(resp);
            }.bind(this));
        },

        render: function () {
            this.templateReady.done(function () {
                var context = this.model.toJSON();
                // calculate some additional state variables
                // because of mustache limitations
                context.create_mode = this.model.isNew();  // model has no id
                context.resolved_status = this.model.get('status') === models.STATES.RESOLVED;
                if (context.resolved_status) {
                    this.$el.addClass('bb-list-item-resolved');
                }
                if (context.status == models.STATES.PENDING) {
                    this.$el.addClass('bb-list-item-pending');
                }

                this.$el.html(this.template(context));
            }.bind(this));

            return this;
        },

        handleTextChange: function (ev) {
            this.model.attributes.text = this.$('textarea').val();
        },

        handleSave: function (ev) {
            ev.preventDefault();
            if (!this.model.isValid()) return;
            this.model.save({
                success: function () {
                    console.log('saved successfully');
                },
                error: function () {
                    console.error('creation failure');
                }
            });
        },

        handleCancel: function (ev) {
            ev.preventDefault();
            this.model.collection.remove(this.model);
        },

        handleRemove: function (ev) {
            var self = this,
                coll = this.model.collection;
            this.model.destroy({
                success: function () {
                    coll.remove(self.model);
                },
                error: function () {
                    console.error('removing failure');
                }
            });
        },

        handleSwitchStatus: function (ev) {
            var toState = {};
            toState[models.STATES.NEW] = models.STATES.RESOLVED;
            toState[models.STATES.RESOLVED] = models.STATES.NEW;

            this.model.save({status: toState[this.model.get('status')]}, {
                success: function () {
                    //console.log('status changed');
                },
                error: function () {
                    console.log('status change failure');
                }
            });

        }

    });

    return TodoItemView;
});